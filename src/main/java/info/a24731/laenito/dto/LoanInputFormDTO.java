package info.a24731.laenito.dto;

import info.a24731.laenito.model.Borrower;
import info.a24731.laenito.model.Loan;
import lombok.Builder;
import lombok.Data;

import javax.validation.Valid;

@Data
@Builder
public class LoanInputFormDTO {
  @Valid
  private final Loan loan;
  @Valid
  private final Borrower borrower;

  public LoanInputFormDTO() {
    this.loan = Loan.empty();
    this.borrower = Borrower.empty();
  }

  public LoanInputFormDTO(Loan loan, Borrower borrower) {
    this.loan = loan;
    this.borrower = borrower;
  }

  public static LoanInputFormDTO empty() {
    LoanInputFormDTO dto = LoanInputFormDTO.builder()
      .loan(Loan.empty())
      .borrower(Borrower.empty())
      .build();
    return dto;
  }
}
