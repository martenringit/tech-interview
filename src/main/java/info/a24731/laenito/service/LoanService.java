package info.a24731.laenito.service;

import info.a24731.laenito.dto.LoanInputFormDTO;
import info.a24731.laenito.model.Borrower;
import info.a24731.laenito.model.Loan;
import info.a24731.laenito.repository.BorrowerRepository;
import info.a24731.laenito.repository.LoanRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Slf4j
@Service
public class LoanService {

  private final LoanRepository loanRepository;
  private final BorrowerRepository borrowerRepository;

  @Autowired
  public LoanService(LoanRepository repoLoan, BorrowerRepository repoBorrower) {
    this.loanRepository = repoLoan;
    this.borrowerRepository = repoBorrower;
  }

  @Transactional
  public LoanInputFormDTO getLoan(Long id, String owner) {
    Loan loan = loanRepository.read(id, owner);
    Borrower borrower = borrowerRepository.read(loan.getBorrower());

    return LoanInputFormDTO.builder().loan(loan).borrower(borrower).build();
  }

  @Transactional
  public List<Loan> list(String username) {
    return loanRepository.list(username);
  }

  @Transactional
  public Long create(LoanInputFormDTO dto) {
    Long borrower = borrowerRepository.create(dto.getBorrower());
    dto.getLoan().setBorrower(borrower);
    Long loan = loanRepository.create(dto.getLoan());
    log.debug("created loan {}", loan);
    return loan;
  }

  @Transactional
  public void save(LoanInputFormDTO dto) {
    borrowerRepository.update(dto.getBorrower());
    loanRepository.update(dto.getLoan());
  }

  @Transactional
  public void deleteAll() {
    loanRepository.deleteAll();
    borrowerRepository.deleteAll();
  }
}
