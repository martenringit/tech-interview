package info.a24731.laenito.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Country {
  private Long iso_numeric_code;
  private String name;
  private String iso_alpha_2_code;
}
